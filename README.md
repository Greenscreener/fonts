# Greenscreener's collection of slightly modified fonts
This repository contains some fonts that I liked but lacked some characters I
needed so I had to add them. I make these by hand, in FontForge, usually late in
the night when I decide I just *have to have* some random font in my next
graphics project, even though it lacks a `ř` or an `ů`. I don't own the original
versions, cool people (unlike me) made them. I just glue bezier curves together
in hopes of it not being too noticeable. If you want to use these, take them,
they're yours. 
